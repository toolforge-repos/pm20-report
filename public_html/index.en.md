---
title: PM20 lists and reports via Wikidata
lang: en

---

The lists below are created by live queries to Wikidata, to which the 20th Century Press Archives folders are linked.

## Companies/orgnizations

* [Map of PM20 companies by headquarters location](https://w.wiki/3jRn)
* [List by industry sector, according to NACE code](https://zbw.eu/beta/sparql-lab/?endpoint=https://query.wikidata.org/sparql&hide=1&queryRef=https://api.github.com/repos/zbw/sparql-queries/contents/wikidata/industries_for_pm20_by_nace.rq&hide=1)
  * query takes a few moments
  * list of companies via "cnt" link
  * not all companies are classified by NACE - check also X-NO-NACE industry names down the list

