---
title: PM20 Listen und Auswertungen via Wikidata
lang: en

---

Die folgenden Listen werden durch Abfragen aus Wikidata erzeugt, wo die Mappen des Pressearchivs 20. Jahrhundert mit Datenobjekten verknüpft sind.

## Firmen/institutionen

* [Karte von Firmen nach Ort des Hauptsitzes](https://w.wiki/3jRn)
* [Liste nach Branche (entsprechend des NACE-Codes)](https://zbw.eu/beta/sparql-lab/?hide=1&endpoint=https://query.wikidata.org/sparql&queryRef=https://api.github.com/repos/zbw/sparql-queries/contents/wikidata/industries_for_pm20_by_nace.rq) 
  * Aufbau der Liste dauert einen Moment
  * Auflistung der Firmen unter "cnt"-Link
  * nicht alle Firmen sind nach NACE klassifiziert - s.a. X-NO-NACE-Einträge im zweiten Teil der Liste

